"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".
"""

import django
from django.test import TestCase
from django.urls import reverse
import json
# TODO: Configure your database in settings.py and sync before running tests.

class ViewTest(TestCase):
    """Tests for the application views."""

    if django.VERSION[:2] >= (1, 7):
        # Django 1.7 requires an explicit setup() when running tests in PTVS
        @classmethod
        def setUpClass(cls):
            super(ViewTest, cls).setUpClass()
            django.setup()

    def test_multiply(self):
        """Tests the home page."""
        kwargs = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        url = reverse('math_calculate')

        get_data = {'txt': '2*2',}

        response = self.client.get(url, get_data, **kwargs)
        json_string = response.content
        data = json.loads(str(json_string.decode("utf-8")));
        #res = data['data'];
        res = {'data':4.0};
        self.assertEqual(data.get('data',None),4.0)

    def test_subtraction(self):
        """Tests the home page."""
        kwargs = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        url = reverse('math_calculate')

        get_data = {'txt': '6-4',}

        response = self.client.get(url, get_data, **kwargs)
        json_string = response.content
        data = json.loads(str(json_string.decode("utf-8")));
        #res = data['data'];
        res = {'data':2.0};
        self.assertEqual(data.get('data',None),2.0)
    
    def test_division(self):
        """Tests the home page."""
        kwargs = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        url = reverse('math_calculate')

        get_data = {'txt': '6/4',}

        response = self.client.get(url, get_data, **kwargs)
        json_string = response.content
        data = json.loads(str(json_string.decode("utf-8")));
        #res = data['data'];
        res = {'data':1.5};
        self.assertEqual(data.get('data',None),1.5)
   
    def test_addition(self):
        """Tests the home page."""
        kwargs = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        url = reverse('math_calculate')

        get_data = {'txt': '4+4',}

        response = self.client.get(url, get_data, **kwargs)
        json_string = response.content
        data = json.loads(str(json_string.decode("utf-8")));
        #res = data['data'];
        res = {'data':8.0};
        self.assertEqual(data.get('data',None),8.0)

   