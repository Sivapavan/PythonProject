from pybuilder.core import init, use_plugin

use_plugin("python.core")
use_plugin("python.unittest")
use_plugin("python.install_dependencies")
use_plugin("python.flake8")
use_plugin("python.distutils")
use_plugin("pypi:pybuilder_django_enhanced_plugin")

name = "django_pybuilder_test"
default_task = "publish"

@init
def set_properties(project):
    project.set_property('django_project', 'myproject')
    project.set_property('django_apps', ['myapp', 'myotherapp'])
    project.set_property('django_subpath', 'django_project')